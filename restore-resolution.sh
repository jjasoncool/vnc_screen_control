#!/bin/bash

# 指令非 primary 的螢幕解析度與位置
while IFS=' ' read -r screen resolution position; do
    xrandr --output $screen --mode $resolution --pos $position
done < original_resolution.txt

# 指定 primary 螢幕解析度
read -r screen resolution position < original_resolution_primary.txt
xrandr --output $screen --mode $resolution --pos $position --primary

# 啟動所有螢幕
xrandr --auto
