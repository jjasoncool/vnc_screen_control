#!/bin/bash

output_file="original_resolution.txt"
# 暫存檔案
tmp_file=$(mktemp)

# 保存原始解析度和位置
xrandr | awk '/ connected/ && !/primary/ {print $1 " " $3}' | sed 's/+/\ /g' | awk '{printf "%s %s %sx%s\n", $1, $2, $3, $4}' | while read -r screen resolution position; do
  # 判斷解析度拿掉 x 之後是否為數字
  if [[ ! "${resolution//x/}" =~ ^[0-9]+$ ]]; then
    # 中斷整個 script
    break
  fi
  echo "$screen $resolution $position"
done > "$tmp_file"

if [[ -s "$tmp_file" ]] then
  # 把暫存檔案複製到要輸出的檔案
  cat "$tmp_file" > "$output_file"
else
  exit 1
fi

# 删除暫存檔案
rm "$tmp_file"

xrandr | awk '/ connected/ && /primary/ {print $1 " " $4}' | sed 's/+/\ /g' | awk '{printf "%s %s %sx%s\n", $1, $2, $3, $4}' | while read -r screen resolution position; do
  echo "$screen $resolution $position"
done > original_resolution_primary.txt

# 取得主螢幕的名稱
primary_screen=$(xrandr | awk '/ connected/ && /primary/ {print $1}')

# 關閉除主螢幕外的其他显示器
for screen in $(xrandr -q | awk '/ connected/ {print $1}' | grep -v "$primary_screen"); do
  xrandr --output $screen --off
done

# 將 主螢幕 的解析度設定為 1920x1080
xrandr --output "$primary_screen" --mode 1920x1080 --pos 0x0
